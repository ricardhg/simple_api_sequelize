import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class Home extends Component {
  render() {
    return (
    <>
      <h1>Página de inicio</h1>
      <Link to={'./list'}>
        <button >
            My List
        </button>
      </Link>
    </>
    );
  }
}
export default Home;