import React, { Component } from 'react';


function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}


class List extends Component {
  constructor(props){
    super(props);
    this.state = {
      pokemons: []
    }

    this.getPokemons = this.getPokemons.bind(this);
    this.getPokemons();
  }

  // pedimos los pokemons a la API
  getPokemons() {
    fetch('/api/pokemons')
     .then( res => res.clone().json())
    .then(pokemons => this.setState({ pokemons: pokemons.data }))
    .catch(err => console.log(err))
  }

  render() {
    if (!this.state.pokemons.length){
        return <h2>Cargando datos</h2>;
    }
    let i = 1;
    const listaPokemons = this.state.pokemons.map(
        item => <li key={i++}>{item.nombre}</li>
        );

    return (
      <>
        <h1>Pokemons</h1>
           <ul>
               {listaPokemons}
           </ul>
      </>
    );
  }
}

export default List;