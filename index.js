
const express = require('express');
const logger = require('morgan');
const path = require('path');

const pokemonsRouter = require('./routes/pokemon-controller');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(logger('dev')); //muestra en consola las peticiones Get, Post... recibidas

app.use('/api/pokemons', pokemonsRouter);

// Enviamos cualquier peticion que no sea una de las anteriores hacia el front
// app.get('*', (req,res) =>{
//     let cami = path.join(__dirname+'/frontnano/dist/index.html');
//     console.log(cami);
//     res.sendFile(cami);
// });


//Static file declaration
app.use(express.static(path.join(__dirname, 'frontnano/dist')));

//production mode
if(process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, 'frontnano/dist')));
  //
  app.get('*', (req, res) => {
    res.sendfile(path.join(__dirname = 'frontnano/dist/index.html'));
  })
}
//build mode
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname+'/client/public/index.html'));
})



const port = 5000
app.listen(port, () => console.log(`App listening on port ${port}!`))

