const express = require('express');
const router = express.Router();
const model = require('../models/index');

router.all('/',(req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
    next();
});

router.all('/:xxx',(req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
    next();
});



/* GET llista de pokemons */
router.get('/', function (req, res, next) {
    model.Pokemon.findAll( )
        .then(pokemons => res.json({
            ok: true,
            data: pokemons
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }))
    });


// petición de UN pokemon, con ID
router.get('/:id', function (req, res, next) {
    model.Pokemon.findOne({ where: {id: req.params.id}} )
        // .then(pokemon => pokemon.get({plain: true}))
        .then(pokemon => res.json({
            ok: true,
            data: pokemon
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }))
    });


/* POST pokemon. creamos un nuevo registro.*/
router.post('/', function(req, res, next) {
    model.Pokemon.create(req.body)
    .then((item) => item.save())
    .then((item)=>res.json({ok: true, data:item}))
    .catch((error)=>res.json({ok:false, error}))
});
 

/* PUT pokemon, actualizamos: findOne + update */
router.put('/:id', function(req, res, next) {
    model.Pokemon.findOne({ where: {id: req.params.id}} )
    .then((poke)=>
        poke.update(req.body)
    )
    .then((ret)=> res.json({
        ok: true,
        data: ret
    }))
    .catch(error => res.json({
        ok: false,
        error: error
    }));
});
 
 
/* DELETE pokemon , eliminamos con destroy */
router.delete('/:id', function(req, res, next) {
    model.Pokemon.destroy({ where: {id: req.params.id}} )
    .then((data)=>res.json({ok: true, data}))
    .catch((error)=>res.json({ok:false, error}))
});


module.exports = router;