
'use strict';

module.exports = (sequelize, DataTypes) => {
  const Pokemon = sequelize.define('Pokemon', {
    nombre: DataTypes.STRING,
    caracter: DataTypes.STRING,
    
  }, { tableName: 'pokemons'});
  
  return Pokemon;
};
